### Инструменты ###
* aiohttp
* rethinkdb
* docker-compose
* pytest-aiohttp

### Готовность ###
* docker-compose
* сервис db (в частности rethinkdb)
* сервис app

### Комментарии ###
* UPD: удалось подчинить TDD async/await
