FROM pypy:latest

COPY ./server/requirements.txt /var/www/requirements.txt
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install -r /var/www/requirements.txt
