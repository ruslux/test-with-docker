import rethinkdb

HOST = 'rethinkdb_container'
PORT = 28015


def sync_init_database(connection, db_name):
    try:
        rethinkdb.db_create(db_name).run(connection)
    except rethinkdb.errors.ReqlOpFailedError as exc:
        pass


def sync_init_tables(connection, db_name):
    try:
        rethinkdb.db(db_name).table_create('task').run(connection)
    except rethinkdb.errors.ReqlOpFailedError as exc:
        pass


async def async_init_database(connection, db_name):
    try:
        await rethinkdb.db_create(db_name).run(connection)
    except rethinkdb.errors.ReqlOpFailedError as exc:
        pass


async def async_init_tables(connection, db_name):
    try:
        await rethinkdb.db(db_name).table_create('task').run(connection)
    except TypeError as exc:
        await rethinkdb.db(db_name).table_create('task').run(connection)
    except rethinkdb.errors.ReqlOpFailedError as exc:
        pass


def patch_async_cursor(cursor):
    async def __anext__(self):
        _has_next = await self.fetch_next()
        if _has_next:
            return await self.next()
        else:
            raise StopAsyncIteration()

    async def __aiter__(self):
        return self

    if not hasattr(cursor.__class__, '_patched'):
        cursor.__class__.__anext__ = __anext__
        cursor.__class__.__aiter__ = __aiter__
        setattr(cursor.__class__, '_patched', True)


def prepare_app(db_name):
    _sync_connection = get_sync_connection()
    sync_init_database(_sync_connection, db_name)
    sync_init_tables(_sync_connection, db_name)

    rethinkdb.set_loop_type("asyncio")


async def cursor_to_list(cursor):
    """
    Need for pypy3 (which implement 3.5 python)
    When pypy implement 3.6 python, need replace this func with async list generator
    """
    patch_async_cursor(cursor)

    _list = []

    async for _item in cursor:
        _list.append(_item)

    return _list


def get_sync_connection():
    return rethinkdb.connect(
        host=HOST,
        port=PORT
    )


async def get_async_connection():
    return await rethinkdb.connect(
        host=HOST,
        port=PORT
    )


def clean_task(task):
    _task = dict(task)
    _drop = ['timestamp', 'status', 'response_status', 'response_json', 'response_text']

    for _key in _drop:
        try:
            del _task[_key]
        except KeyError:
            pass

    return _task
