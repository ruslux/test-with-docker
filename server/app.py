import rethinkdb
from aiohttp import web
from aiohttp.web_response import json_response

from server.constants import Status
from server.db_utils import get_async_connection, cursor_to_list, clean_task, prepare_app

DB = None

async def _get(request):
    _id = request.match_info.get('task_id')
    _async_connection = await get_async_connection()

    if _id:
        _task = await rethinkdb.db(DB).table("task").get(_id).run(_async_connection)
        if _task:
            _task = clean_task(_task)
            return json_response(_task)
        else:
            return web.Response(status=404)
    else:
        _tasks_cursor = await rethinkdb.db(DB).table("task").run(_async_connection)
        _tasks = await cursor_to_list(_tasks_cursor)
        _tasks = list(map(lambda x: clean_task(x), _tasks))

        return json_response(_tasks)


async def _post(request):
    try:
        _data = await request.post()
        _async_connection = await get_async_connection()

        _query = {'url': _data['url'], 'interval': _data['interval']}

        _task_exists = await rethinkdb.db(DB).table("task").filter(_query).count().run(_async_connection)

        if _task_exists:
            return web.Response(status=409)
        else:
            _task = dict(_query)
            _task['status'] = Status.IDLE
            _task['timestamp'] = rethinkdb.now()
            await rethinkdb.db(DB).table('task').insert(_task).run(_async_connection)
            return web.Response(status=201)
    except Exception as exc:
        return web.Response(status=500)


async def _put(request):
    try:
        _data = await request.post()   # shit
        _data = clean_task(_data)
        del _data['id']

        _id = request.match_info.get('task_id')
        _async_connection = await get_async_connection()

        _result = await rethinkdb.db(DB).table("task").get(_id).update(_data).run(_async_connection)

        if _result['replaced']:
            return web.Response(status=200)
        elif _result['skipped']:
            return web.Response(status=404)
        else:
            raise ValueError
    except Exception as exc:
        return web.Response(status=500)


async def _delete(request):
    try:
        _id = request.match_info.get('task_id')
        _async_connection = await get_async_connection()
        _result = await rethinkdb.db(DB).table("task").get(_id).delete().run(_async_connection)

        if _result['deleted']:
            return web.Response(status=204)
        elif _result['skipped']:
            return web.Response(status=404)
        else:
            raise ValueError
    except Exception as exc:
        return web.Response(status=500)


def get_web_app(db_name):
    global DB
    DB = db_name

    _app = web.Application()

    _app.router.add_get('/task/', _get)
    _app.router.add_get('/task/{task_id:[a-zA-Z0-9\-]+}/', _get)
    _app.router.add_post('/task/', _post)
    _app.router.add_put('/task/{task_id:[a-zA-Z0-9\-]+}/', _put)
    _app.router.add_delete('/task/{task_id:[a-zA-Z0-9\-]+}/', _delete)

    return _app

if __name__ == "__main__":
    _app = get_web_app('primary_db')
    prepare_app(DB)
    web.run_app(_app)
