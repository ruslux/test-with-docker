class Status(object):
    IDLE = 'idle'
    RUNNING = 'running'
    FAILED = 'failed'
    STOPPED = 'stopped'
