import rethinkdb
from aiohttp import client
import asyncio

from server.constants import Status
from server.db_utils import get_async_connection, prepare_app, cursor_to_list


async def process():
    _connection = await get_async_connection()

    while True:
        _now = rethinkdb.now()

        _tasks = await rethinkdb.db('primary_db').table('task').run(_connection)
        _tasks = await cursor_to_list(_tasks)

        for _task in _tasks:
            if _task['timestamp'] >= _now + _task['interval']:

                _task['status'] = Status.RUNNING

                await rethinkdb.db('primary_db').table('task').get(_task['id']).update(_task).run(_connection)
                _response = await client.request('GET', _task['url'])

                _task['response_status'] = _response.status
                _task['response_json'] = _response.status
                _task['response_text'] = await _response.text()
                _task['timestamp'] = _now
                _task['status'] = Status.IDLE

                await rethinkdb.db('primary_db').table('task').get(_task['id']).update(_task).run(_connection)


if __name__ == "__main__":
    prepare_app('primary_db')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(process())
