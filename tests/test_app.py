import rethinkdb
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from server.app import get_web_app
from server.db_utils import get_async_connection, async_init_database, async_init_tables


class AppTestCase(AioHTTPTestCase):
    async def get_application(self):
        rethinkdb.set_loop_type("asyncio")

        _connection = await get_async_connection()
        _app = get_web_app('test_app')
        await async_init_database(_connection, 'test_app')
        await async_init_tables(_connection, 'test_app')
        return _app

    async def clean_db(self):
        _connection = await get_async_connection()
        await rethinkdb.db_drop('test_app').run(_connection)

    def tearDown(self):
        self.loop.run_until_complete(self.clean_db())
        super().tearDown()

    @unittest_run_loop
    async def test_empty_db_get_without_id(self):
        _request = await self.client.request("GET", "/task/")
        assert _request.status == 200
        _response = await _request.json()
        assert _response == []

    @unittest_run_loop
    async def test_empty_db_get_with_id(self):
        _request = await self.client.request("GET", "/task/1/")
        assert _request.status == 404

    @unittest_run_loop
    async def test_post_and_both_get(self):
        _data = {'url': 'https://google.com', 'interval': '100'}

        _request = await self.client.request("POST", "/task/", data=_data)
        assert _request.status == 201

        _request = await self.client.request("GET", "/task/")
        assert _request.status == 200
        _response = await _request.json()
        _data['id'] = _response[0]['id']
        assert _response == [_data]

        _request = await self.client.request("GET", "/task/{}/".format(_data['id']))
        assert _request.status == 200
        _response = await _request.json()
        assert _response == _data

    @unittest_run_loop
    async def test_put(self):
        _data = {'url': 'https://google.com', 'interval': '100'}

        _request = await self.client.request("POST", "/task/", data=_data)
        assert _request.status == 201

        _request = await self.client.request("GET", "/task/")
        assert _request.status == 200
        _response = await _request.json()

        _data['id'] = _response[0]['id']
        _data['url'] = 'https://yandex.ru'

        _request = await self.client.request("PUT", "/task/{}/".format(_data['id']), data=_data)
        assert _request.status == 200

        _request = await self.client.request("PUT", "/task/unknown/", data=_data)
        assert _request.status == 404

    @unittest_run_loop
    async def test_delete(self):
        _data = {'url': 'https://google.com', 'interval': '100'}

        _request = await self.client.request("POST", "/task/", data=_data)
        assert _request.status == 201

        _request = await self.client.request("GET", "/task/")
        assert _request.status == 200
        _response = await _request.json()

        _request = await self.client.request("DELETE", "/task/{}/".format(_response[0]['id']))
        assert _request.status == 204

        _request = await self.client.request("GET", "/task/")
        assert _request.status == 200
        _response = await _request.json()
        assert _response == []

        _request = await self.client.request("DELETE", "/task/unknown/")
        assert _request.status == 404
